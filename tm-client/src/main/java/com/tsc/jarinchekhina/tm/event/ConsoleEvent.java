package com.tsc.jarinchekhina.tm.event;

/**
 * ConsoleEvent
 *
 * @author Yuliya Arinchekhina
 */
public class ConsoleEvent {

    private String name;

    public ConsoleEvent(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
