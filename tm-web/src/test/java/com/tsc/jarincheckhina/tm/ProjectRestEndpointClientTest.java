//package com.tsc.jarincheckhina.tm;
//
//import com.tsc.jarincheckhina.tm.marker.IntegrationCategory;
//import com.tsc.jarinchekhina.tm.client.ProjectRestEndpointClient;
//import com.tsc.jarinchekhina.tm.model.Project;
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import org.junit.After;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.experimental.categories.Category;
//
//import java.util.List;
//
///**
// * ProjectRestEndpointClientTest
// *
// * @author Yuliya Arinchekhina
// */
//public class ProjectRestEndpointClientTest {
//
//    @NotNull
//    final ProjectRestEndpointClient client = ProjectRestEndpointClient.client();
//
//    @NotNull
//    final Project projectFirst = new Project("FIRST");
//
//    @NotNull
//    final Project projectSecond = new Project("SECOND");
//
//    @NotNull
//    final Project projectThird = new Project("THIRD");
//
//    @Before
//    public void init() {
//        client.save(projectFirst);
//        client.save(projectSecond);
//        client.save(projectThird);
//    }
//
//    @After
//    public void clean() {
//        client.clear();
//    }
//
//    @Test
//    @Category(IntegrationCategory.class)
//    public void testFindAll() {
//        @Nullable final List<Project> actual = client.findAll();
//        Assert.assertNotNull(actual);
//        Assert.assertEquals(3, actual.size());
//    }
//
//    @Test
//    @Category(IntegrationCategory.class)
//    public void testSave() {
//        @NotNull final Project project = new Project("ZERO");
//        @Nullable final Project actual = client.save(project);
//        Assert.assertNotNull(actual);
//        Assert.assertEquals(project.getName(), actual.getName());
//    }
//
//    @Test
//    @Category(IntegrationCategory.class)
//    public void testFindById() {
//        @Nullable final Project project = client.save(new Project("ZERO"));
//        Assert.assertNotNull(project);
//        @Nullable final Project actual = client.findById(project.getId());
//        Assert.assertNotNull(actual);
//        Assert.assertEquals(project.getName(), actual.getName());
//    }
//
//    @Test
//    @Category(IntegrationCategory.class)
//    public void testDeleteById() {
//        @Nullable final Project project = client.save(new Project("ZERO"));
//        Assert.assertNotNull(project);
//        client.deleteById(project.getId());
//        @Nullable final Project actual = client.findById(project.getId());
//        Assert.assertNull(actual);
//    }
//
//}
