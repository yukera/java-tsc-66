//package com.tsc.jarincheckhina.tm;
//
//import com.tsc.jarincheckhina.tm.marker.IntegrationCategory;
//import com.tsc.jarinchekhina.tm.client.TaskRestEndpointClient;
//import com.tsc.jarinchekhina.tm.model.Task;
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import org.junit.After;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.experimental.categories.Category;
//
//import java.util.List;
//
///**
// * TaskRestEndpointClientTest
// *
// * @author Yuliya Arinchekhina
// */
//public class TaskRestEndpointClientTest {
//
//    @NotNull
//    final TaskRestEndpointClient client = TaskRestEndpointClient.client();
//
//    @NotNull
//    final Task taskFirst = new Task("FIRST");
//
//    @NotNull
//    final Task taskSecond = new Task("SECOND");
//
//    @NotNull
//    final Task taskThird = new Task("THIRD");
//
//    @Before
//    public void init() {
//        client.save(taskFirst);
//        client.save(taskSecond);
//        client.save(taskThird);
//    }
//
//    @After
//    public void clean() {
//        client.clear();
//    }
//
//    @Test
//    @Category(IntegrationCategory.class)
//    public void testFindAll() {
//        @Nullable final List<Task> actual = client.findAll();
//        Assert.assertNotNull(actual);
//        Assert.assertEquals(3, actual.size());
//    }
//
//    @Test
//    @Category(IntegrationCategory.class)
//    public void testSave() {
//        @NotNull final Task task = new Task("ZERO");
//        @Nullable final Task actual = client.save(task);
//        Assert.assertNotNull(actual);
//        Assert.assertEquals(task.getName(), actual.getName());
//    }
//
//    @Test
//    @Category(IntegrationCategory.class)
//    public void testFindById() {
//        @Nullable final Task task = client.save(new Task("ZERO"));
//        Assert.assertNotNull(task);
//        @Nullable final Task actual = client.findById(task.getId());
//        Assert.assertNotNull(actual);
//        Assert.assertEquals(task.getName(), actual.getName());
//    }
//
//    @Test
//    @Category(IntegrationCategory.class)
//    public void testDeleteById() {
//        @Nullable final Task task = client.save(new Task("ZERO"));
//        Assert.assertNotNull(task);
//        client.deleteById(task.getId());
//        @Nullable final Task actual = client.findById(task.getId());
//        Assert.assertNull(actual);
//    }
//
//}
