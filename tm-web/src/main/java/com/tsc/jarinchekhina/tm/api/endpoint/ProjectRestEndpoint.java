package com.tsc.jarinchekhina.tm.api.endpoint;

import com.tsc.jarinchekhina.tm.model.Project;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * ProjectRestEndpoint
 *
 * @author Yuliya Arinchekhina
 */
public interface ProjectRestEndpoint {

    @GetMapping("/findAll")
    List<Project> findAll();

    @PostMapping("/save")
    Project save(@RequestBody Project project);

    @GetMapping("/findById/{id}")
    Project findById(@PathVariable("id") String id);

    @GetMapping("/existsById/{id}")
    boolean existsById(@PathVariable("id") String id);

    @GetMapping("/count")
    long count();

    @PostMapping("/deleteById/{id}")
    void deleteById(@PathVariable("id") String id);

    @PostMapping("/delete")
    void delete(@RequestBody List<Project> projectList);

    @PostMapping("/clear")
    void clear();

}
