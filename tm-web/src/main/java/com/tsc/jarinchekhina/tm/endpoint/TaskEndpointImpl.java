package com.tsc.jarinchekhina.tm.endpoint;

import com.tsc.jarinchekhina.tm.api.endpoint.TaskRestEndpoint;
import com.tsc.jarinchekhina.tm.model.Task;
import com.tsc.jarinchekhina.tm.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * TaskEndpointImpl
 *
 * @author Yuliya Arinchekhina
 */
@RestController
@RequestMapping("/api/tasks")
public class TaskEndpointImpl implements TaskRestEndpoint {

    @Autowired
    private TaskRepository taskRepository;

    @Override
    @GetMapping("/findAll")
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    @PostMapping("/save")
    public Task save(@RequestBody Task task) {
        return taskRepository.save(task);
    }

    @Override
    @GetMapping("/findById/{id}")
    public Task findById(@PathVariable("id") String id) {
        return taskRepository.findById(id).orElse(null);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@PathVariable("id") String id) {
        return taskRepository.existsById(id);
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return taskRepository.count();
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") String id) {
        taskRepository.deleteById(id);
    }

    @Override
    @PostMapping("/delete")
    public void delete(@RequestBody List<Task> taskList) {
        taskRepository.deleteAll(taskList);
    }

    @Override
    @PostMapping("/clear")
    public void clear() {
        taskRepository.deleteAll();
    }

}
