package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * ProjectRepository
 *
 * @author Yuliya Arinchekhina
 */

public interface ProjectRepository extends JpaRepository<Project, String> {
}
