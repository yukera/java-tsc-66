package com.tsc.jarinchekhina.tm.client;

import com.tsc.jarinchekhina.tm.model.Task;
import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * TaskRestEndpointClient
 *
 * @author Yuliya Arinchekhina
 */
public interface TaskRestEndpointClient {

    String BASE_URL = "http://localhost/8080/api/project/";

    static TaskRestEndpointClient client() {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                    .contract(new SpringMvcContract())
                    .encoder(new SpringEncoder(objectFactory))
                    .decoder(new SpringDecoder(objectFactory))
                    .target(TaskRestEndpointClient.class, BASE_URL);

    }

    @GetMapping("/findAll")
    List<Task> findAll();

    @PutMapping("/save")
    Task save(@RequestBody final Task project);

    @GetMapping("/findById/{id}")
    Task findById(@PathVariable("id") final String id);

    @DeleteMapping("/deleteById/{id}")
    void deleteById(@PathVariable("id") final String id);

    @DeleteMapping("/clear")
    void clear();

}
