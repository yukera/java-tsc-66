package com.tsc.jarinchekhina.tm.endpoint;

import com.tsc.jarinchekhina.tm.api.endpoint.ProjectRestEndpoint;
import com.tsc.jarinchekhina.tm.model.Project;
import com.tsc.jarinchekhina.tm.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * ProjectEndpointImpl
 *
 * @author Yuliya Arinchekhina
 */
@RestController
@RequestMapping("/api/projects")
public class ProjectEndpointImpl implements ProjectRestEndpoint {

    @Autowired
    private ProjectRepository projectRepository;

    @Override
    @GetMapping("/findAll")
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    @PostMapping("/save")
    public Project save(@RequestBody Project project) {
        return projectRepository.save(project);
    }

    @Override
    @GetMapping("/findById/{id}")
    public Project findById(@PathVariable("id") String id) {
        return projectRepository.findById(id).orElse(null);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@PathVariable("id") String id) {
        return projectRepository.existsById(id);
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return projectRepository.count();
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") String id) {
        projectRepository.deleteById(id);
    }

    @Override
    @PostMapping("/delete")
    public void delete(@RequestBody List<Project> projectList) {
        projectRepository.deleteAll(projectList);
    }

    @Override
    @PostMapping("/clear")
    public void clear() {
        projectRepository.deleteAll();
    }

}
