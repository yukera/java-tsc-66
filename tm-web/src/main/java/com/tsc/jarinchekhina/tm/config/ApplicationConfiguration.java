package com.tsc.jarinchekhina.tm.config;

import org.springframework.context.annotation.ComponentScan;

/**
 * ApplicationConfiguration
 *
 * @author Yuliya Arinchekhina
 */
@ComponentScan("com.tsc.jarinchekhina.tm")
public class ApplicationConfiguration {
}
