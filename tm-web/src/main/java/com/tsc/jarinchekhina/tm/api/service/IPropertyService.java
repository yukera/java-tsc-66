package com.tsc.jarinchekhina.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    String getJdbcDriver();

    @NotNull
    String getJdbcPassword();

    @NotNull
    String getJdbcUrl();

    @NotNull
    String getJdbcUsername();

    @NotNull
    String getHibernateDialect();

    @NotNull
    String getHibernateHbm2ddl();

    @NotNull
    String getHibernateShow();

    @NotNull
    String getHibernateLazyLoad();

    @NotNull
    String getHibernateSecondLevelCache();

    @NotNull
    String getHibernateQueryCache();

    @NotNull
    String getHibernateMinimalPuts();

    @NotNull
    String getHibernateLiteMember();

    @NotNull
    String getHibernateRegionPrefix();

    @NotNull
    String getHibernateConfigFile();

    @NotNull
    String getHibernateRegionFactoryClass();

}
