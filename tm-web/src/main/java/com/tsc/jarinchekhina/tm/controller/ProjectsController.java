package com.tsc.jarinchekhina.tm.controller;

import com.tsc.jarinchekhina.tm.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * ProjectsController
 *
 * @author Yuliya Arinchekhina
 */
@Controller
public class ProjectsController {

    @Autowired
    private ProjectRepository projectRepository;

    @GetMapping("/projects")
    public ModelAndView index() {
        return new ModelAndView("project-list", "projects", projectRepository.findAll());
    }

}
