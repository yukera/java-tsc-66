package com.tsc.jarinchekhina.tm.enumerated;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Status
 *
 * @author Yuliya Arinchekhina
 */
@Getter
@AllArgsConstructor
public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    private final String displayName;

}
