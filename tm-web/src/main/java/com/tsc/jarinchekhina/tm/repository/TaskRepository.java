package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * TaskRepository
 *
 * @author Yuliya Arinchekhina
 */
public interface TaskRepository extends JpaRepository<Task, String> {
}