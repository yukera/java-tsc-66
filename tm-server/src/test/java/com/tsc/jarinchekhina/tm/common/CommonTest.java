//package com.tsc.jarinchekhina.tm.common;
//
//import lombok.Getter;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.context.ApplicationContext;
//
///**
// * CommonTest
// *
// * @author Yuliya Arinchekhina
// */
//@Getter
//@SpringBootTest
//public abstract class CommonTest {
//
//    @Autowired
//    protected ApplicationContext context;
//
//}
