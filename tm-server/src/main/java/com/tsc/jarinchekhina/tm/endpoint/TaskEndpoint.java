package com.tsc.jarinchekhina.tm.endpoint;

import com.tsc.jarinchekhina.tm.api.endpoint.ITaskEndpoint;
import com.tsc.jarinchekhina.tm.api.service.IProjectTaskService;
import com.tsc.jarinchekhina.tm.api.service.ISessionService;
import com.tsc.jarinchekhina.tm.api.service.ITaskService;
import com.tsc.jarinchekhina.tm.api.service.IUserService;
import com.tsc.jarinchekhina.tm.dto.SessionDTO;
import com.tsc.jarinchekhina.tm.dto.TaskDTO;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.model.Task;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Controller
@WebService
@NoArgsConstructor
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private IProjectTaskService projectTaskService;

    @NotNull
    @Override
    @WebMethod
    public TaskDTO bindTaskByProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String taskId
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.USER);
        return Task.toDTO(projectTaskService.bindTaskByProjectId(session.getUserId(), projectId, taskId));
    }

    @Override
    @WebMethod
    public void changeTaskStatusById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.USER);
        taskService.changeTaskStatusById(session.getUserId(), id, status);
    }

    @Override
    @WebMethod
    public void changeTaskStatusByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.USER);
        taskService.changeTaskStatusByName(session.getUserId(), name, status);
    }

    @Override
    @WebMethod
    public void clearTasks(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.USER);
        taskService.clear(session.getUserId());
    }

    @Override
    @WebMethod
    public void createTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.USER);
        taskService.create(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void createTaskWithDescription(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.USER);
        taskService.create(session.getUserId(), name, description);
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDTO> findAllTaskByProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.USER);
        return Task.toDTO(projectTaskService.findAllTaskByProjectId(session.getUserId(), projectId));
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDTO> findAllTasks(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.USER);
        return Task.toDTO(taskService.findAll(session.getUserId()));
    }

    @NotNull
    @Override
    @WebMethod
    public TaskDTO findTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.USER);
        return Task.toDTO(taskService.findById(session.getUserId(), id));
    }

    @NotNull
    @Override
    @WebMethod
    public TaskDTO findTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.USER);
        return Task.toDTO(taskService.findByName(session.getUserId(), name));
    }

    @Override
    @WebMethod
    public void finishTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.USER);
        taskService.finishTaskById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void finishTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.USER);
        taskService.finishTaskByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void removeTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.USER);
        taskService.removeById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.USER);
        taskService.removeByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void startTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.USER);
        taskService.startTaskById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void startTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.USER);
        taskService.startTaskByName(session.getUserId(), name);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskDTO unbindTaskByProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String taskId
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.USER);
        return Task.toDTO(projectTaskService.unbindTaskByProjectId(session.getUserId(), taskId));
    }

    @Override
    @WebMethod
    public void updateTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.USER);
        taskService.updateTaskById(session.getUserId(), id, name, description);
    }

}
