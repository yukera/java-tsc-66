package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.dto.SessionDTO;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface SessionRepository extends AbstractRepositoryDTO<SessionDTO> {

    @NotNull
    SessionDTO save(@NotNull final SessionDTO session);

    void deleteAll();

    @NotNull
    List<SessionDTO> findAll();

    @NotNull
    Optional<SessionDTO> findFirstById(@NotNull final String id);

    @NotNull
    List<SessionDTO> findAllByUserId(@NotNull final String userId);

    void delete(@NotNull final SessionDTO session);

    void deleteById(@NotNull final String id);

}
