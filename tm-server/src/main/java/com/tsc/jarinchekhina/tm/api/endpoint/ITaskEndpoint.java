package com.tsc.jarinchekhina.tm.api.endpoint;

import com.tsc.jarinchekhina.tm.api.IEndpoint;
import com.tsc.jarinchekhina.tm.dto.SessionDTO;
import com.tsc.jarinchekhina.tm.dto.TaskDTO;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskEndpoint extends IEndpoint<Task> {

    void clearTasks(@Nullable SessionDTO session);

    @NotNull
    List<TaskDTO> findAllTasks(@Nullable SessionDTO session);

    void createTask(@Nullable SessionDTO session, @Nullable String name);

    void createTaskWithDescription(@Nullable SessionDTO session, @Nullable String name, @Nullable String description);

    @NotNull
    TaskDTO findTaskById(@Nullable SessionDTO session, @Nullable String id);

    @NotNull
    TaskDTO findTaskByName(@Nullable SessionDTO session, @Nullable String name);

    void removeTaskById(@Nullable SessionDTO session, @Nullable String id);

    void removeTaskByName(@Nullable SessionDTO session, @Nullable String name);

    void updateTaskById(
            @Nullable SessionDTO session,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    void startTaskById(@Nullable SessionDTO session, @Nullable String id);

    void startTaskByName(@Nullable SessionDTO session, @Nullable String name);

    void finishTaskById(@Nullable SessionDTO session, @Nullable String id);

    void finishTaskByName(@Nullable SessionDTO session, @Nullable String name);

    void changeTaskStatusById(@Nullable SessionDTO session, @Nullable String id, @Nullable Status status);

    void changeTaskStatusByName(@Nullable SessionDTO session, @Nullable String name, @Nullable Status status);

    @NotNull
    List<TaskDTO> findAllTaskByProjectId(@Nullable SessionDTO session, @Nullable String projectId);

    @NotNull
    TaskDTO bindTaskByProjectId(@Nullable SessionDTO session, @Nullable String projectId, @Nullable String taskId);

    @NotNull
    TaskDTO unbindTaskByProjectId(@Nullable SessionDTO session, @Nullable String taskId);

}
