package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.model.AbstractEntity;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Scope("prototype")
public interface AbstractRepository<E extends AbstractEntity> extends JpaRepository<E, String> {
}
