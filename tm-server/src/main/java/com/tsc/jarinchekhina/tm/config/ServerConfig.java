package com.tsc.jarinchekhina.tm.config;

import com.tsc.jarinchekhina.tm.api.service.IPropertyService;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * ServerConfig
 *
 * @author Yuliya Arinchekhina
 */
@Configuration
@EnableTransactionManagement
@ComponentScan("com.tsc.jarinchekhina.tm")
@EnableJpaRepositories("com.tsc.jarinchekhina.tm.repository")
public class ServerConfig {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Bean
    @NotNull
    public DataSource dataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(propertyService.getJdbcDriver());
        dataSource.setUrl(propertyService.getJdbcUrl());
        dataSource.setUsername(propertyService.getJdbcUsername());
        dataSource.setPassword(propertyService.getJdbcPassword());
        return dataSource;
    }

    @Bean
    public PlatformTransactionManager transactionManager(@NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory) {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(@NotNull final DataSource dataSource) {
        final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("com.tsc.jarinchekhina.tm.model", "com.tsc.jarinchekhina.tm.dto");
        @NotNull final Properties properties = new Properties();
        properties.put(Environment.DIALECT, propertyService.getHibernateDialect());
        properties.put(Environment.HBM2DDL_AUTO, propertyService.getHibernateHbm2ddl());
        properties.put(Environment.SHOW_SQL, propertyService.getHibernateShow());
        properties.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getHibernateSecondLevelCache());
        properties.put(Environment.USE_QUERY_CACHE, propertyService.getHibernateQueryCache());
        properties.put(Environment.USE_MINIMAL_PUTS, propertyService.getHibernateMinimalPuts());
        properties.put(Environment.CACHE_REGION_PREFIX, propertyService.getHibernateRegionPrefix());
        properties.put(Environment.CACHE_REGION_FACTORY, propertyService.getHibernateRegionFactoryClass());
        properties.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getHibernateConfigFile());
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

}
